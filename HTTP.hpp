#pragma once
#include <boost/asio.hpp>
#include <boost/beast.hpp>
#include <string_view>

namespace Http {
	std::pair<bool, std::string> GET(const std::string& host, const std::string& port, const std::string& target) {
		namespace beast = boost::beast;
		namespace http = beast::http;
		using tcp = boost::asio::ip::tcp;

		// GET
		try {
			// HTTP 1.1
			const auto version = 11;

			http::request<http::string_body> req(http::verb::get, target, version);

			// name resolution
			boost::asio::io_context ioc;
			tcp::resolver resolver(ioc);
			auto result = resolver.resolve(host, port);
			beast::tcp_stream stream(ioc);
			stream.connect(result);

			req.set(http::field::host, host);
			req.set(http::field::user_agent, BOOST_BEAST_VERSION_STRING);

			// send GET request
			http::write(stream, req);

			beast::flat_buffer buffer;
			http::response<http::string_body> res;

			http::read(stream, buffer, res);

			beast::error_code error;
			stream.socket().shutdown(tcp::socket::shutdown_both, error);

			if (error && error != beast::errc::not_connected)
				throw beast::system_error{ error };

			return std::make_pair(true, res.body());
		}
		catch (std::exception const& e) {
			return std::make_pair(false, e.what());
		}
	}


	std::pair<bool, std::string> POST(const std::string& host, const std::string& port, const std::string& target, const std::string& body_data) {
		namespace beast = boost::beast;
		namespace http = beast::http;
		using tcp = boost::asio::ip::tcp;

		// POST
		try {
			// https://host:port/target
			const auto version = 11;

			http::request<http::string_body> req(http::verb::post, target, version);

			// name resolution
			boost::asio::io_context ioc;
			tcp::resolver resolver(ioc);
			auto result = resolver.resolve(host, port);
			beast::tcp_stream stream(ioc);
			stream.connect(result);

			req.set(http::field::host, host);
			req.set(http::field::user_agent, BOOST_BEAST_VERSION_STRING);

			// POST data in body
			req.set(http::field::body, body_data);

			// send POST request
			http::write(stream, req);

			// read response
			beast::flat_buffer buffer;
			http::response<http::string_body> res;
			http::read(stream, buffer, res);

			beast::error_code error;
			stream.socket().shutdown(tcp::socket::shutdown_both, error);

			if (error && error != beast::errc::not_connected)
				throw beast::system_error{ error };

			return std::make_pair(true, res.body());
		}
		catch (std::exception const& e) {
			return std::make_pair(false, e.what());
		}
	}
}