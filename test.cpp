#include <iostream>
#include "HTTP.hpp"

int main() {
	auto [get_result, get_body] = GET("localhost", "3000", "/index");
	if (!get_result) {
		std::cout << "Error:" << get_body << std::endl;
	}
	else {
		std::cout << "Success:" << get_body << std::endl;
	}

	auto [post_result, post_body] = POST("localhost", "3000", "/index", "some_data");
	if (!post_result) {
		std::cout << "Error:" << post_body << std::endl;
	}
	else {
		std::cout << "Success:" << post_body << std::endl;
	}

}